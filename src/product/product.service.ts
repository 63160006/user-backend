import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  {
    id: 1,
    name: 'admin',
    price: 500,
  },
  {
    id: 2,
    name: 'ธันวา แสนสุด',
    price: 200,
  },
  {
    id: 3,
    name: 'สมพร สมรศรี',
    price: 100,
  },
];

let lastProductId = 4;

@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto, //login, name, password
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('product ' + JSON.stringify(products[index]));
    // console.log('update ' + JSON.stringify(updateProductDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'admin', price: 500 },
      { id: 2, name: 'ธันวา', price: 200 },
      { id: 3, name: 'แสนสุด', price: 100 },
    ];
    const lastProductIdId = 4;
    return 'RESET';
  }
}
